package riccmp.com.pe.sumiprint;

public interface PrinterCallback {
    String getResult();
    void onReturnString(String result);
}
